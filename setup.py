#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# Notes for myself

## Basic usage

In general all you need to do is running `setup.py` -- and you are ready to go
But you may be interested in installing some plugins for additional functionality

## How to use builtin NeoVim package manager

Plus my proposals of plugins to install

```bash
rm -rf ~/.local/share/nvim/site/pack/downloaded_addons/start
mkdir -p ~/.local/share/nvim/site/pack/downloaded_addons/start
cd ~/.local/share/nvim/site/pack/downloaded_addons/start

git clone https://github.com/godlygeek/tabular
git clone https://github.com/preservim/vim-markdown
git clone https://github.com/junegunn/goyo.vim
git clone https://github.com/Shatur/neovim-ayu
git clone https://github.com/navarasu/onedark.nvim
git clone https://github.com/folke/tokyonight.nvim
git clone https://github.com/EdenEast/nightfox.nvim
git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/airblade/vim-gitgutter
git clone https://github.com/junegunn/fzf
git clone https://github.com/junegunn/fzf.vim
git clone https://github.com/phaazon/hop.nvim
git clone https://github.com/neovim/nvim-lspconfig
git clone https://github.com/nvim-treesitter/nvim-treesitter
git clone https://github.com/ms-jpq/coq_nvim
git clone https://github.com/ms-jpq/coq.artifacts

git clone https://github.com/simrat39/rust-tools.nvim
git clone https://github.com/nvim-lua/plenary.nvim
git clone https://github.com/mfussenegger/nvim-dap

sed -i 's/^colorscheme/" colorscheme/' ~/.config/nvim/init.vim ; sed -i 's/^" let g:onedark/let g:onedark/' ~/.config/nvim/init.vim
```

Path for windows:

```cmd
md %userprofile%\\AppData\\Local\\nvim-data\\site\\pack\\downloaded_addons\\start
cd %userprofile%\\AppData\\Local\\nvim-data\\site\\pack\\downloaded_addons\\start
```

All paths can be listed by this command:

```vimscript
:help standard-path
```

pyright should be installed separately using your favorite package manager
```bash
conda install --yes pyright black
```

You may need to install dependensies for coq_nvim
```vimscript
:COQdeps
:COQnow -s
```
"""

import os
from pathlib import Path
import shutil
import urllib.request

if os.name == "posix":
    folder_path = Path.home() / ".config/nvim/"
else:
    folder_path = Path.home() / "AppData/Local/nvim/"

THEMES = {
    "https://raw.githubusercontent.com/NLKNguyen/papercolor-theme/master/colors/PaperColor.vim": "PaperColor.vim",
    "https://raw.githubusercontent.com/sonph/onehalf/master/vim/colors/onehalfdark.vim": "onehalfdark.vim",
    "https://raw.githubusercontent.com/fratajczak/one-monokai-vim/master/colors/one-monokai.vim": "one_monokai.vim",
    "https://raw.githubusercontent.com/ayu-theme/ayu-vim/master/colors/ayu.vim": "ayu.vim",
}

repo_url = "https://gitlab.com/XelorR/friendly_vim/-/raw/master/"
init_vim = {"url": f"{repo_url}init.vim", "filename": folder_path / "init.vim"}

for f in ["colors", "syntax"]:
    if not (folder_path / f).exists():
        (folder_path / f).mkdir(parents=True)


class AppURLopener(urllib.request.FancyURLopener):
    version = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.69 Safari/537.36"


urllib._urlopener = AppURLopener()
urllib._urlopener.retrieve(init_vim["url"], init_vim["filename"])

for t_path, t_name in THEMES.items():
    urllib._urlopener.retrieve(t_path, folder_path / "colors" / t_name)

# vim part

urllib._urlopener.retrieve(
    "https://gitlab.com/XelorR/friendly_vim/-/raw/master/.vimrc", Path.home() / ".vimrc"
)

urllib._urlopener.retrieve(
    "https://gitlab.com/XelorR/friendly_vim/-/raw/master/.exrc", Path.home() / ".exrc"
)

if not (Path.home() / ".vim" / "colors").exists():
    (Path.home() / ".vim" / "colors").mkdir(parents=True)

for f in os.listdir(folder_path / "colors"):
    shutil.copy(folder_path / "colors" / f, Path.home() / ".vim" / "colors" / f)

# enabling downloaded theme for NeoVim

for config_filepath in [folder_path / "init.vim", Path.home() / ".vimrc"]:

    with open(config_filepath, "r") as f:
        init_vim = f.read()

    with open(config_filepath, "w") as f:
        f.write(init_vim.replace('" colorscheme', "colorscheme"))

# ayu theme fix

for config_filepath in [folder_path / "colors/ayu.vim", Path.home() / ".vim/colors/ayu.vim"]:

    with open(config_filepath, "r") as f:
        ayu_legacy = f.read()

    with open(config_filepath, "w") as f:
        f.write(ayu_legacy.replace('let &background = s:style', '" let &background = s:style'))

