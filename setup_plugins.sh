#!/usr/bin/env sh

# required latest nvim
# sh ./get_neovim.sh # in case if your Linux distro is using something from dimasaur era

mkdir -p ~/.local/share/nvim/site/pack/downloaded_addons/start
cd ~/.local/share/nvim/site/pack/downloaded_addons/start
curl -sL https://gitlab.com/XelorR/friendly_vim/-/raw/master/setup.py | grep "git clone" | bash
cd ~

