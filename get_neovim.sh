#!/usr/bin/env sh

NVIM=~/opt/nvim.appimage

mkdir -p ~/opt
if [ ! -f $NVIM ]; then
    wget --output-document $NVIM https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
    chmod u+x $NVIM
    echo 'alias nvim="~/opt/nvim.appimage"' >> ~/.bashrc
fi

