" dependensies: ripgrep, python, (everything is optional)

" configure expanding of tabs for various file types
au BufRead,BufNewFile *.py set expandtab
au BufRead,BufNewFile *.c set noexpandtab
au BufRead,BufNewFile *.cpp set noexpandtab
au BufRead,BufNewFile *.mod set noexpandtab
au BufRead,BufNewFile *.go set noexpandtab
au BufRead,BufNewFile *.h set noexpandtab
au BufRead,BufNewFile *.txt set noexpandtab
au BufRead,BufNewFile *.md set expandtab
au BufRead,BufNewFile *.ini set noexpandtab
au BufRead,BufNewFile *.cfg set noexpandtab
au BufRead,BufNewFile Makefile* set noexpandtab

set termguicolors
set background=light
let ayucolor="mirage"
" colorscheme ayu
syntax enable
set foldmethod=indent
set nofoldenable
set tabstop=4
set shiftwidth=4
set expandtab
set number
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrap
set linebreak
set nolist
set conceallevel=2
let g:vim_markdown_conceal_code_blocks = 0
set nofixendofline

" splits windows in more habitual manner
set splitright
set splitbelow

" enable mouse scroll, windows select and cursor move
set mouse=a

" if you have ripgrep, use it instead of default command
if executable('rg')
  set grepprg=rg\ --vimgrep\ --smart-case\ --hidden
  set grepformat=%f:%l:%c:%m
  tnoremap <C-u> :Rg<CR>
  nnoremap <C-u> :Rg<CR>
  inoremap <C-u> :Rg<CR>
endif

" if you have fzf plugin installed, apply hotkey
if executable('fzf') " assuming that plugin is also installed
  tnoremap <C-p> :FZF<CR>
  nnoremap <C-p> :FZF<CR>
  inoremap <C-p> :FZF<CR>
endif

" Ignore some paths while fuzzy-searching
set wildignore+=*/.git/*,*/tmp/*,*.swp

" === Key bindings =================================

" defining leader key
let mapleader = "\<Space>"

" --- Leader ---------------------------------------

" b - buffer management
nnoremap <leader>b :ls<CR>:b<Space>
noremap <leader>e :browse old!<CR>
noremap <A-f> :let @" = expand("%")<CR>
noremap <leader>f :let @" = expand("%")<CR>
noremap ffff :let @" = expand("%")<CR>
noremap <leader>w :w<Return>

" n and m - note taking in zettelkasten manner
" Proof of concept. To refactor

noremap <leader>h :exe 'e '.strftime("%Y-%m-%d").'.md'<CR>
noremap M-h :exe 'e '.strftime("%Y-%m-%d").'.md'<CR>
noremap <leader>nn :exe 'e '.strftime("%Y%m%d%H%M%S").'.md'
noremap <leader>n' :exe 'vsplit '.strftime("%Y%m%d%H%M%S").'.md'
noremap <leader>n- :exe 'split '.strftime("%Y%m%d%H%M%S").'.md'
nnoremap <leader>mm :call ConnectNoteNormalMode()<CR>
vnoremap <leader>mm :call ConnectNoteVisualMode()<CR>
nnoremap <leader>m- :call ConnectNoteNormalModeSplit()<CR>
vnoremap <leader>m- :call ConnectNoteVisualModeSplit()<CR>
nnoremap <leader>m' :call ConnectNoteNormalModeVsplit()<CR>
vnoremap <leader>m' :call ConnectNoteVisualModeVsplit()<CR>

function! ConnectNoteNormalMode()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal a[n](n)
    write
    execute "e ".getreg('n')
    normal i[c](c)
	normal o
	normal o
endfunction

function! ConnectNoteVisualMode()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal gv"hy
    normal `>a](n)
    normal `<i[
    write
    execute "e ".getreg('n')
    normal i[c](c)
	normal o
	normal o# h
	normal o
	normal o
endfunction

function! ConnectNoteNormalModeSplit()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal a[n](n)
    write
    execute "split ".getreg('n')
    normal i[c](c)
	normal o
	normal o
endfunction

function! ConnectNoteVisualModeSplit()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal gv"hy
    normal `>a](n)
    normal `<i[
    write
    execute "split ".getreg('n')
    normal i[c](c)
	normal o
	normal o# h
	normal o
	normal o
endfunction

function! ConnectNoteNormalModeVsplit()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal a[n](n)
    write
    execute "vsplit ".getreg('n')
    normal i[c](c)
	normal o
	normal o
endfunction

function! ConnectNoteVisualModeVsplit()
    let @n=strftime("%Y%m%d%H%M%S").'.md'
    let @c=expand("%")
    normal gv"hy
    normal `>a](n)
    normal `<i[
    write
    execute "vsplit ".getreg('n')
    normal i[c](c)
	normal o
	normal o# h
	normal o
	normal o
endfunction

" t - terminal
nnoremap <leader>tt :terminal<CR>

if has('win32')
  nnoremap <leader>t' :vert term cmd<CR><C-w>p
  nnoremap <leader>t- :term cmd<CR><C-w>p
elseif executable("fish")
  nnoremap <leader>t' :vert term fish<CR><C-w>p
  nnoremap <leader>t- :term fish<CR><C-w>p
elseif executable("zsh")
  nnoremap <leader>t' :vert term zsh<CR><C-w>p
  nnoremap <leader>t- :term zsh<CR><C-w>p
elseif executable("bash")
  nnoremap <leader>t' :vert term bash<CR><C-w>p
  nnoremap <leader>t- :term bash<CR><C-w>p
else
  nnoremap <leader>t' :vert term sh<CR><C-w>p
  nnoremap <leader>t- :term sh<CR><C-w>p
endif

" python hotkeys

" python console
if executable("bpython")
  nnoremap <leader>gg :term ++curwin bpython<CR>
  nnoremap <leader>g- :term bpython<CR><C-w>p
  nnoremap <leader>g' :vert term bpython<CR><C-w>p
elseif executable("python3")
  nnoremap <leader>gg :term ++curwin python3<CR>
  nnoremap <leader>g- :term python3<CR><C-w>p
  nnoremap <leader>g' :vert term python3<CR><C-w>p
else
  nnoremap <leader>gg :term ++curwin python<CR>
  nnoremap <leader>g- :term python<CR><C-w>p
  nnoremap <leader>g' :vert term python<CR><C-w>p
endif

" --- Alt ------------------------------------------

" fixing Alt mapping
for i in range(97,122)
  let c = nr2char(i)
  exec "map \e".c." <M-".c.">"
  exec "map! \e".c." <M-".c.">"
endfor

" REPL, sending text to neovim terminal
nnoremap <A-s> "tyy<C-w>w"tpa<CR><C-\><C-n><C-w>pj
nnoremap <leader>s "tyy<C-w>w"tpa<CR><C-\><C-n><C-w>pj
inoremap <A-s> <C-[>"tyy<C-w>w"tpa<CR><C-\><C-n><C-w>pjI
vnoremap <A-s> "ty<C-w>w"tpa<CR><CR><C-\><C-n><C-w>p
vnoremap <leader>s "ty<C-w>w"tpa<CR><CR><C-\><C-n><C-w>p
nnoremap <A-S> ?[%`]\{2,\}<CR>jV/[%`]\{2,\}<CR>k"ty<C-w>w"tpa<CR><CR><C-\><C-n><C-w>p:<C-u>nohlsearch<CR><C-l>``

" buffers
" movements
inoremap <C-k> <C-[>:bprev!<Return>
inoremap <C-j> <C-[>:bnext!<Return>
noremap <C-k> :bprev!<Return>
noremap <C-j> :bnext!<Return>
tnoremap <C-k> <C-\><C-n>:bprev!<Return>
tnoremap <C-j> <C-\><C-n>:bnext!<Return>
" force close any buffer
nnoremap <C-q> :bdelete!<Return>
tnoremap <C-q> <C-\><C-n>:bdelete!<Return>
inoremap <C-q> <C-[>:bdelete!<Return>
" switch windows
nnoremap <M-n> <C-w><C-w>
tnoremap <M-n> <C-\><C-n><C-w><C-w>
inoremap <M-n> <C-[><C-w><C-w>
nnoremap <leader><tab> <C-w><C-w>
tnoremap <leader><tab> <C-\><C-n><C-w><C-w>
inoremap <leader><tab> <C-[><C-w><C-w>
" create splits
nnoremap <leader>- :split<CR>
nnoremap <leader>' :vsplit<CR>
" switch rows
vnoremap <C-Up> :m-2<CR>gv
vnoremap <C-Down> :m'>+<CR>gv
nnoremap <C-Up> :m-2<CR>
nnoremap <C-Down> :m+1<CR>
inoremap <C-Up> <C-[>:m-2<CR>
inoremap <C-Down> <C-[>:m+1<CR>

" insert date and time
nnoremap <A-g> a<C-r>=strftime("%Y-%m-%d")<CR>
inoremap <A-g> <C-r>=strftime("%Y-%m-%d")<CR>
inoremap dddd <C-r>=strftime("%Y-%m-%d")<CR>
nnoremap <A-T> a<C-r>=strftime("%H:%M")<CR>
inoremap <A-T> <C-r>=strftime("%H:%M")<CR>
inoremap tttt <C-r>=strftime("%H:%M")<CR>
nnoremap <A-t> a<C-r>=strftime("%Y%m%d%H%M%S")<CR>
inoremap <A-t> <C-r>=strftime("%Y%m%d%H%M%S")<CR>
inoremap ssss <C-r>=strftime("%Y%m%d%H%M%S")<CR>

" --- Control --------------------------------------

" buffers
" movements
inoremap <C-PageUp> <C-[>:bprev!<Return>
inoremap <C-PageDown> <C-[>:bnext!<Return>
noremap <C-PageUp> :bprev!<Return>
noremap <C-PageDown> :bnext!<Return>
tnoremap <C-PageUp> <C-\><C-n>:bprev!<Return>
tnoremap <C-PageDown> <C-\><C-n>:bnext!<Return>
" noremap <C-Up> <C-y>
" noremap <C-Down> <C-e>

" --- Mixed ----------------------------------------

" cd to current file
noremap <leader>cd :cd %:p:h<cr>

" yank and paste using system clipboard
noremap <leader>y "+y
noremap <leader>Y "+Y
noremap <leader>p "+p
noremap <leader>P "+P

" Seamlessly treat visual lines as actual lines when moving around.
noremap j gj
noremap k gk
noremap <Down> gj
noremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" exit to normal mode from insert with 'jj' and 'esc'
inoremap jj <c-[>
tnoremap jj <c-\><c-n>
tnoremap <esc> <c-\><c-n>
tnoremap <c-v><esc> <esc>

" reset highlight and redraw theme on screen refresh
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

" To simulate |i_CTRL-R| in terminal-mode: >
tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'

" force exit
noremap <leader>qq :q!<CR>

" run macro in visual selecthion
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>
function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

" From http://got-ravings.blogspot.com/2008/07/vim-pr0n-visual-search-mappings.html
" makes * and # work on visual mode too.
function! s:VSetSearch(cmdtype)
  let temp = @s
  norm! gv"sy
  let @/ = '\V' . substitute(escape(@s, a:cmdtype.'\'), '\n', '\\n', 'g')
  let @s = temp
endfunction

xnoremap * :<C-u>call <SID>VSetSearch('/')<CR>/<C-R>=@/<CR><CR>
xnoremap # :<C-u>call <SID>VSetSearch('?')<CR>?<C-R>=@/<CR><CR>

" recursively vimgrep for word under cursor or selection if you hit leader-star
if maparg('<leader>*', 'n') == ''
  nmap <leader>* :execute 'noautocmd vimgrep /\V' . substitute(escape(expand("<cword>"), '\'), '\n', '\\n', 'g') . '/ **'<CR>
endif
if maparg('<leader>*', 'v') == ''
  vmap <leader>* :<C-u>call <SID>VSetSearch()<CR>:execute 'noautocmd vimgrep /' . @/ . '/ **'<CR>
endif
