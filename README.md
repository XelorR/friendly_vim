# Friendly Vim

NeoVim/Vim configs which I use on daily basis. (Currently NO)

> UPDATE: currenrly I use [Emacs config](https://gitlab.com/xelorr/notemacs) on top of [Spacemacs](https://spacemacs.org) with [Org Roam](https://www.orgroam.com) for managing notes. It perfectly covers all my needs, so this repo currently unmaintained.

## My personal Editor War results:

> Know the purpose

- [NeoVim](https://neovim.io) - general usage text and script editor, [thinking, planning and note taking app](https://telegra.ph/Zettelcasten-notes---core-principles-12-25), universal REPL tool
- [Codium](https://vscodium.com) - general usage code editor and data science environment
- [CudaText](https://cudatext.github.io) - default text files viewer
- [IDE from JetBrains](https://www.jetbrains.com/products/#type=ide-vs) - language specific debugger and refactorer

> UPDATE: currenrly Emacs for **Everything** but [VSCode](https://vscode.dev) for jupyters and remote development. I also rely  on [CudaText](https://cudatext.github.io) to view very large files.

## Key config ideas:

- lightweight and highly portable - [init.vim](init.vim) or [.vimrc](.vimrc) is enough (but it is still extensible with [plugins](setup_with_plugins.sh))
- installable with easy to remember oneliner
- leader key shortcuts inspired by [SpaceMacs](https://www.spacemacs.org)
- work with multiple buffers without plugins: convenient buffer navigation shortcuts
- builtin terminal is awsome, let's configure REPL on top of it
- making notes is better with Vim, so let's adapt our editor to [Zettelkasten](https://telegra.ph/Zettelcasten-notes---core-principles-12-25)
- NeoVim is the best, but let's replicate the same behavior for Vim also

## Quick setup

```{bash}
curl -L https://gitlab.com/XelorR/friendly_vim/-/raw/master/setup.py | python3
```

or via it's **easy to remember alias**
```{bash}
curl -L https://is.gd/friendly_vim | python3
```

windows version:
```{powershell}
Invoke-WebRequest -Uri https://is.gd/friendly_vim -OutFile nvim_setup.py; python3 nvim_setup.py; rm nvim_setup.py
```

Works for Linux, MacOS, Windows (run [setup.py](setup.py) with python3) and [Termux](https://termux.com) (unix-like terminal for Android).
May work on other systems like FreeBSD but never been tested.
Depends on python 3.6+. But still... you can just copy [init.vim](init.vim) or [.vimrc](.vimrc) from this repo.

## Direct downloads

- [init.vim](https://gitlab.com/XelorR/friendly_vim/-/raw/master/init.vim) - for neovim (recommended)
- [.vimrc](https://gitlab.com/XelorR/friendly_vim/-/raw/master/.vimrc) - for vim
- [.exrc](https://gitlab.com/XelorR/friendly_vim/-/raw/master/.exrc) - for minimal vim, preinstalled on most linux distros

## Key binding

- SPC = Space
- A = Alt
- M = Alt
- C = Ctrl
- PgDn = Page Down
- PgUp = Page Up
- Tab = Tab
- Esc = Escape

Please note that there some difficulties with Alt button usage on Macs, so most of hotkeys are not using Alt for compatibility reasons.

### Misc

| Key     | Behaviour                                                   |
|---------|-------------------------------------------------------------|
| SPC b   | list opened buffers and celect one to jump to               |
| SPC e   | list most recently opened files and select one to open      |
| SPC w   | write buffer to disk                                        |
| SPC cd  | change directory to current file's                          |
| SPC -   | create new vindow for current file in horisontal split      |
| SPC '   | create new vindow for current file in vertical split        |
| C-q     | kill current buffer                                         |
| SPC q q | force quit                                                  |
| SPC f b | format current buffer with black (if in PATH) - python formatter |
| A-n     | cycle through opened splits/windows (Linux and Windows only)|
| SPC Tab | cycle through opened splits/windows                         |
| C-j     | switch to next buffer                                       |
| C-k     | switch to previous buffer                                   |
| C-PgDn  | switch to next buffer                                       |
| C-Pgup  | switch to previous buffer                                   |
| C-Up    | move current line up                                        |
| C-Down  | move currelnt line down                                     |
| SPC y   | copy to system buffer                                       |
| SPC Y   | copy line to system buffer                                  |
| SPC p   | paste from system buffer, after cursor                      |
| SPC P   | paste from system buffer, before cursor                     |
| jj      | Terurn to normal mode from insert and terminal modes        |
| C-l     | redraw inferface and switch off search results highlighting |
| SPC *   | recursively vimgrep word or selection under cursor          |
| ESC     | Return trom ANY mode to normal mode                         |
| C-v ESC | Send Esc to terminal                                        |

### Terminals and REPL

| Key     | Behaviour                                                                  |
|---------|----------------------------------------------------------------------------|
| SPC t t | open new shell in current window                                           |
| SPC t - | open new shell in horisontal split                                         |
| SPC t ' | open new shell in vertical split                                           |
| SPC g g | open new python console in current window                                  |
| SPC g - | open new python console in horisontal split                                |
| SPC g ' | open new python console in vertical split                                  |
| A-s     | send row or visual selection to opened console (should be opened in split) |
| SPC s   | send row or visual selection to opened console (should be opened in split) |
| A-S     | send markdown code block to opened console (should be opened in split)     |

### Managing Zettelkasten notes

| Key      | Behaviour                                                                            |
|----------|--------------------------------------------------------------------------------------|
| A-f      | copy current file relative path to unnamed buffer (Linux and Windows only)           |
| SPC f    | copy current file relative path to unnamed buffer                                    |
| ffff     | copy current file relative path to unnamed buffer                                    |
| ssss     | insert timestamp (input mode)                                                        |
| tttt     | insert current time (input mode)                                                     |
| dddd     | insert current date (input mode)                                                     |
| SPC n n  | create new timestamp-named note, edit in current window                              |
| SPC n -  | create new timestamp-named note, edit in horisontal split                            |
| SPC n '  | create new timestamp-named note, edit in vertical split                              |
| SPC m m  | create new timestamp-named note, connect with current note, edit in current window   |
| SPC m -  | create new timestamp-named note, connect with current note, edit in horisontal split |
| SPC m '  | create new timestamp-named note, connect with current note, edit in vertical split   |
| SPC h    | open daily note                                                                      |
| A-h    | open daily note                                                                      |
| gf       | open file under cursor in current window (Vim default)                               |
| C-w f    | open file under cursor in horisontal split (Vim default)                             |
| C-w v gf | open file under cursor in vertical split (Vim default)                               |
| C-w c    | Close current window (not buffer) (Vim default)                                      |
| C-w o    | Close other windows (not buffers) (Vim default)                                      |
| g C-a    | (on visual selection) increase each row number like previous but +1 (NeoVim default) |
