au BufRead,BufNewFile *.py set expandtab
au BufRead,BufNewFile *.c set noexpandtab
au BufRead,BufNewFile *.cpp set noexpandtab
au BufRead,BufNewFile *.mod set noexpandtab
au BufRead,BufNewFile *.go set noexpandtab
au BufRead,BufNewFile *.h set noexpandtab
au BufRead,BufNewFile *.txt set noexpandtab
au BufRead,BufNewFile *.md set expandtab
au BufRead,BufNewFile *.ini set noexpandtab
au BufRead,BufNewFile *.cfg set noexpandtab
au BufRead,BufNewFile Makefile* set noexpandtab

set foldmethod=indent
set nofoldenable
set tabstop=4
set shiftwidth=4
set expandtab
set number
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrap
set linebreak
set nolist

set splitright
set splitbelow
nnoremap <Space>- :split<CR>
nnoremap <Space>' :vsplit<CR>

nnoremap <Space>b :ls<CR>:b<Space>
noremap <Space>w :w<Return>
nnoremap <C-q> :bdelete!<Return>
inoremap <C-q> <C-[>:bdelete!<Return>
noremap <Space>qq :q!<CR>

inoremap <C-k> <C-[>:bprev!<Return>
inoremap <C-j> <C-[>:bnext!<Return>
noremap <C-k> :bprev!<Return>
noremap <C-j> :bnext!<Return>
inoremap <C-PageUp> <C-[>:bprev!<Return>
inoremap <C-PageDown> <C-[>:bnext!<Return>
noremap <C-PageUp> :bprev!<Return>
noremap <C-PageDown> :bnext!<Return>

noremap <Space>cd :cd %:p:h<cr>

noremap j gj
noremap k gk
noremap <Down> gj
noremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

inoremap jj <c-[>

vnoremap <C-Up> :m-2<CR>gv
vnoremap <C-Down> :m'>+<CR>gv
nnoremap <C-Up> :m-2<CR>
nnoremap <C-Down> :m+1<CR>
inoremap <C-Up> <C-[>:m-2<CR>
inoremap <C-Down> <C-[>:m+1<CR>

vnoremap ge y:e<Space>0<CR>

